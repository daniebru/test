include 'docker'

class {'docker::compose':
  ensure => present,
  version => '3.2.0',
}

docker_compose { 'test':
  compose_files => [ 'docker-compose.yaml' ],
  ensure => present,
  before => Docker::Stack[ 'swarm' ],
}

docker::stack { 'swarm':
  ensure  => present,
   stack_name => 'swarm',
  compose_files => ['docker-compose.yaml'],
  require => [Class['docker']], 
}

