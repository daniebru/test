include 'docker'

class {'docker::compose':
  ensure => present,
  version => latest,
  before => Docker_compose['test'],
}

docker_compose { 'test':
  compose_files => ['docker-compose.yaml'],
  ensure  => present,
  scale => {
	'manager' => 3,
	},
}


