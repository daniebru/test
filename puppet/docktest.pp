include 'docker'

docker::image  {'base' :
  image_tag => 'docktest',
  before => Docker::Run['docktest'],
}

docker::run { 'docktest':
  image   => 'base',
  ensure => present,
  command => '/bin/sh -c "while true; do echo hello world; sleep 1; done"',
}
